// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  imports: {
    dirs: ["stores"],
  },
  vite: {
    server: {
      watch: {
        usePolling: true,
      },
    },
  },
  modules: ["@nuxtjs/tailwindcss", "@vueuse/nuxt"],
});
